<?php
// +----------------------------------------------------------------------
// | Yzncms [ 御宅男工作室 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://yzncms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 御宅男 <530765310@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 开发者：klay
// +----------------------------------------------------------------------
namespace app\adv\controller;
use app\common\controller\Adminbase;
use think\Db;
use think\facade\Session;
use Config\app;
use app\common\model\WxImg as WxImg_Model;



class WxImg extends Adminbase
{

    //初始化
    protected function initialize()
    {
        parent::initialize();
        $this->WxImg_Model = new WxImg_Model;

        //取得当前内容模型模板存放目录
        $this->filepath = TEMPLATE_PATH . (empty(config('theme')) ? "default" : config('theme')) . DIRECTORY_SEPARATOR . "cms" . DIRECTORY_SEPARATOR;
        //取得栏目频道模板列表
        $this->tp_category = str_replace($this->filepath . DIRECTORY_SEPARATOR, '', glob($this->filepath . DIRECTORY_SEPARATOR . 'category*'));
        //取得栏目列表模板列表
        $this->tp_list = str_replace($this->filepath . DIRECTORY_SEPARATOR, '', glob($this->filepath . DIRECTORY_SEPARATOR . 'list*'));
        //取得内容页模板列表
        $this->tp_show = str_replace($this->filepath . DIRECTORY_SEPARATOR, '', glob($this->filepath . DIRECTORY_SEPARATOR . 'show*'));
        //取得单页模板
        $this->tp_page = str_replace($this->filepath . DIRECTORY_SEPARATOR, '', glob($this->filepath . DIRECTORY_SEPARATOR . 'page*'));
    }

    //列表数据
    public function index()
    {
        if ($this->request->isAjax()){
            $uid = Session::get('admin_user_auth.uid');
            //设置没页数量
            $limit = $this->request->param('limit/d', 10);
            //设置分页
            $page = $this->request->param('page/d', 10);

            $_list = $this->WxImg_Model->where('uid',$uid)->order(['id' => 'desc'])->page($page, $limit)->select();
            $total = $_list->count();
            $result = array("code" => 0, "count" => $total, "data" => $_list);
            return json($result);
        }
        return $this->fetch();
    }

    //新增
    public function Add()
    {
        $uid = Session::get('admin_user_auth.uid');
       
        //判断提交方式
        if ($this->request->isPost()) {
            //POST 请求接口

            //获取表单提交的数据
            $data = $this->request->post();
            $data['uid']=$uid;
            //创建时间
            $data['create_time'] =date('Y-m-d H:i:s',time());
            $data['status'] ='1';
            //获取微信素材id
            $data['thumb_media_id'] = add_material('/www/wwwroot/guanggao/public'.$data['img']);
            //插入数据
//            var_dump($data['thumb_media_id']);exit;
            if(WxImg_Model::insert($data)){

                $this->success("添加成功","WxImg/index");
            }else{
                $this->error("添加失败");
            }
        }else {
            //GET 方式
        }
        return $this->fetch();
    }

    //编辑
    public function Edit()
    {

        $id = $this->request->param('id/d', 0);

        if ($this->request->isPost()) {
            //POST 保持数据
            if (empty($id)) {
                $this->error('请指定需要修改的记录！');
            }
            //获取表单提交的数据
            $data = $this->request->post();
            //获取微信素材id
            if($data['img_old'] !== $data['img']){
                $data['thumb_media_id'] = add_material('/www/wwwroot/guanggao/public'.$data['img']);
            }
            unset($data['img_old']);
            //执行修改
            echo '<pre>';
            print_r($data);exit;
            if(WxImg_Model::where("id","{$id}")->update($data)){
                $this->success("修改成功","WxImg/index");
            }else{
                $this->error("修改失败");
            }


        } else {
            //打开编辑页面
            //获取编辑对于记录的id
            if (empty($id)) {
                $this->error('请指定需要修改的记录！');
            }
            $data = WxImg_Model::get($id);
            //查询到的数据，传到view模板
            $this->assign("data", $data);
            return $this->fetch();

        }

    }

    //删除单条记录
    public function Delete()
    {

        //a 代表接收的多个id是数组，如果是d 代表int
        $ids = $this->request->param('ids/a', null);
        if (empty($ids)) {
            $this->error('参数错误！');
        }

        if (!is_array($ids)) {
            //得到id 不是数组
            $ids = array(0 => $ids);
        }

        //异常处理 try   (try百度)
        try {
            //处理正常的代码
            foreach ($ids as $id) {
                WxImg_Model::where("id","{$id}")->delete();
            }

        } catch (\Exception $ex) {
            //如果有错误，就会提示具体错误信息
            $this->error($ex->getMessage());
        }


        $this->success("删除成功！");

    }


}
