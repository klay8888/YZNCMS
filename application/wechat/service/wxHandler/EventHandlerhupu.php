<?php
/**
 *
 */
namespace app\wechat\service\wxHandler;

use \EasyWeChat\Kernel\Contracts\EventHandlerInterface;
use \EasyWeChat\Kernel\Messages\News;
use \EasyWeChat\Kernel\Messages\NewsItem;
use \EasyWeChat\Kernel\Messages\Text;

class EventHandlerhupu implements EventHandlerInterface
{
    //$payload 载荷为msg内容主体
    public function handle($payload = null)
    {
        $message = ' ';
        var_dump("expression");exit();
        switch ($payload['Event']) {
            case 'SCAN':
                $message = $this->scan($payload);
                break;
            case 'subscribe':
                $is_scan = isset($payload['EventKey']); //是否有扫描二维码事件
                $message = $this->subscribe($payload); //所有都要进行关注逻辑
                if ($is_scan) {
                    $this->scan($payload); //扫码逻辑
                }
                break;
            case 'unsubscribe':
                $message = $this->unsubscribe();
                break;
            default:
                # code...
                break;
        }

        return $message;

    }

    public function scan($payload)
    {

    }

    public function subscribe($payload)
    {  
        $wechatServer = app('wechatServer');   
        $wechatServer->officialAccount->customer_service->message($this->getTextMsg())->to($payload['FromUserName'])->send();
        $wechatServer->officialAccount->customer_service->message($this->getNewsItem())->to($payload['FromUserName'])->send();

    }

    public function unsubscribe($payload)
    {

    }
    //文本加超链接
    public function getTextMsg()
    {
        $arr = [
            ['title' => '旅游不会拍照？学会这几招，怎么拍都好看！', 'url' => 'https://t.1yb.co/1yPG'],
            ['title' => '小桥流水怎么拍？花海怎么拍？技巧来咯！！', 'url' => 'https://t.1yb.co/1yOW'],
            ['title' => '汉服出行拍照速成指南', 'url' => 'https://t.1yb.co/1yNX'],
            ['title' => '想拍出惊艳的照片？这份古镇拍照技巧一定要收下！！', 'url' => 'https://t.1yb.co/1yOn'],
        ];
        $html = '';
        if (is_array($arr)) {
            foreach ($arr as $k => $v) {
                $html .= "\r\n<a href='" . $v['url'] . "'>→ " . $v['title'] . "</a>\r\n"; //a标签的前后不要加上html的其他标签，例如空格&nbsp;之类的
            }
        }

        $news = new Text($html);
        return $html;

    }

    //多图文信息只能一条
    public function getNewsItem()
    {
        $items = [
            new NewsItem([
                'title'       => '111',
                'description' => '...华埔专业华埔专业华埔专业华埔专业华埔专业华埔专业华埔专业华埔专业华埔专业华埔专业华埔专业华埔专业华埔专业',
                'url'         => 'www.baidu.com',
                'image'       => 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1595495135349&di=3228ce70f8ab8ad01d1b54f82b265f8c&imgtype=0&src=http%3A%2F%2Fa3.att.hudong.com%2F14%2F75%2F01300000164186121366756803686.jpg',
                // ...
            ]),
        ];
        $news = new News($items);
        return $news;
    }
}
