<?php
/**
 *
 */
namespace app\wechat\service\wxHandler;

use EasyWeChat\Kernel\Messages\Text;
use \EasyWeChat\Kernel\Contracts\EventHandlerInterface;

class TextHandler implements EventHandlerInterface
{
    //$payload 载荷为msg内容主体
    public function handle($payload = null)
    {   
        $message = new Text('Hello world!');
        return $message;
    }
}
