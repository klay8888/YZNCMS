<?php
/*
 * author Lhq
 * 使用方法 在provider.php 里面绑定这个类 依赖注入后即可app()全局调用
 * composer 引入 composer require overtrue/wechat:~4.0 -vvv
 */
namespace app\wechat\service;

use EasyWeChat\Factory;

class workServer extends Server
{

    public $enterprise; //企业微信

    public function __construct()
    {
        //企业微信通用应用配置
        $enterpriseConfig = [
            'corp_id'       => 'wwc212091ff4fd4449',
            //'agent_id'      => ' ', // 如果有 agend_id 则填写
            'secret'        => 'PLhCEp7H72D0XHdC2ustaCFY-yY-nZzNm2XObEpN8sU',
            'token'         => 'n6AoBXfDV913UnxRWohh8HWpg7qpgNOs',
            'aes_key'       => 'bWycBFdRkVchiF3xzddnTBxsBUUDLCAweEBIGvSMG8E',
            'response_type' => 'array',
        ];
        $this->enterprise = Factory::work($enterpriseConfig);
        //企业微信通讯录应用配置
        $maillistConfig = [
            'corp_id'       => 'wwc212091ff4fd4449',
            //'agent_id'      => ' ', // 如果有 agend_id 则填写
            'secret'        => 'xZPpZqawYflIw5Vhm6YflKj5HGa6SdE3ft9SVsBzHy0',
            'token'         => 'n6AoBXfDV913UnxRWohh8HWpg7qpgNOs',
            'aes_key'       => 'bWycBFdRkVchiF3xzddnTBxsBUUDLCAweEBIGvSMG8E',
            'response_type' => 'array',
        ];
        $this->enterprise = Factory::work($enterpriseConfig);
        $this->maillist   = Factory::work($maillistConfig);

    }
    //创建企业用户
    public function createUser($data)
    {
        $result = $this->maillist->user->create($data);
        return $this->returnFormat($result);
    }
    //获取可联系外部用户的成员
    public function getFollowUsers()
    {
        $result = $this->enterprise->external_contact->getFollowUsers();
        return $this->returnFormat($result);
    }
    //添加联系我方式
    public function addContactWay($userId, $state)
    {
        //参数1是 联系方式 1 单人 2 多人 参数2是 联系方式 1小程序 2是二维码
        $result = $this->enterprise->contact_way->create(1, 2, ['state' => $state, 'user' => [$userId]]);
        return $this->returnFormat($result);
    }
    //更新联系我方式 之后要重新下载二维码图片
    public function updateContactWay($configId, $state)
    {
        $result = $this->enterprise->contact_way->update($configId, ['state' => $state]);
        return $this->returnFormat($result);
    }
    //获取联系我方式
    public function getContactWay($configId)
    {
        $result = $this->enterprise->contact_way->get($configId);
        return $this->returnFormat($result);
    }
    //获取客户列表
    public function getContactList($userId)
    { 
        $result = $this->enterprise->external_contact->list($userId); //用户详情
        return $this->returnFormat($result);
    }
    //获取客户详情
    public function getExternalContact($externalUserId)
    {
        $result = $this->enterprise->external_contact->get($externalUserId); //用户详情
        return $this->returnFormat($result);
    }
    //获取所有企业微信用户
    public function getUser($userId)
    {
        $result = $this->enterprise->user->get($userId);
        return $this->returnFormat($result);
    }
    //获取部门列表
    public function getDepartment()
    {
        $result = $this->enterprise->department->list();
        return $this->returnFormat($result);
    }
    //递归获取部门下面成员
    public function getDepartmentUsers($departmentId)
    {
        $result = $this->enterprise->user->getDepartmentUsers($departmentId, true);
        return $this->returnFormat($result);
    }

}
