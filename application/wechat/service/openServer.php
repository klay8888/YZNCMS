<?php
/*
 * author Lhq
 * 使用方法 在provider.php 里面绑定这个类 依赖注入后即可app()全局调用
 * composer 引入 composer require overtrue/wechat:~4.0 -vvv
 */
namespace app\wechat\service;

use \EasyWeChat\Factory;

class openServer extends Server
{
    public $official; //公众号名字
    public $openPlatform; //开放平台
    public static $HUPU = 'wx401553255b893aed';

    public function __construct()
    {

        $openPlatformConfig = [
            'app_id'        => 'wx3f8db8c6dd056596',
            'secret'        => '57be5e7ddf178e30b42cf29cd195b8e3',
            'token'         => 'g.huapuscom',
            'response_type' => 'array',
            'aes_key'       => 'xdhpbhtjcs11lfjyswggp8vmxfzhxq5l6amt2hwbzn8',
            'expire'     => 0,
            'log'           => [
                'default'  => 'dev', // 默认使用的 channel，生产环境可以改为下面的 prod
                'channels' => [
                    // 测试环境
                    'dev'  => [
                        'driver' => 'single',
                        'path'   => '/tmp/easywechat.log',
                        'level'  => 'debug',
                    ],
                    // 生产环境
                    'prod' => [
                        'driver' => 'daily',
                        'path'   => '/tmp/easywechat.log',
                        'level'  => 'info',
                    ],
                ],
            ],
        ];
        $this->openPlatform     = Factory::openPlatform($openPlatformConfig);
    }

   public function createOfficialAccount($appid)
   {  
       $oauth = open_token();
       $this->official = $this->openPlatform->officialAccount($appid,$oauth);    
   }
    //获取用户信息
    public function getUser($openId, $name)
    {
        $result = $this->official->user->get($openId);
        return $this->returnFormat($result);
    }
    //生成临时二维码
    public function tempQrCode($data, $name)
    {
        $result = $this->official->qrcode->temporary($data, 6 * 24 * 3600);
        return $this->returnFormat($result);
    }
    //生成永久二维码
    public function foreverQrCode($data, $name)
    {
        $result = $this->official->qrcode->forever($data); // 或者 $app->qrcode->forever("foo");
        return $this->returnFormat($result);
    }
    //获取上传图片
    public function uploadWxImage($filePath)
    {
        $result = $this->official->material->uploadImage($filePath);
        return $this->returnFormat($result);
    }

}
