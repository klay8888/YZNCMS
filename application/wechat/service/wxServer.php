<?php
/*
 * author Lhq
 * 使用方法 在provider.php 里面绑定这个类 依赖注入后即可app()全局调用
 * composer 引入 composer require overtrue/wechat:~4.0 -vvv
 */
namespace app\wechat\service;

use \EasyWeChat\Factory;

class wxServer extends Server
{
    public $officialAccount; //华埔公众号
    public function __construct()
    {
        $hupuAccountConfig = [
            'app_id'        => 'wx401553255b893aed',
            'secret'        => '21ceaaa8696a093852b96043590847ce',
            'token'         => 'IiQ0xT102Qk1aifQ5bfADH2fh5aIvaiI',
            'response_type' => 'array',
            'oauth'         => [
                'scopes'   => ['snsapi_userinfo'], //回调授权级别
                'callback' => '/index/wechat/oauth', //回调地址
            ],
        ];

        $this->officialAccount = Factory::officialAccount($hupuAccountConfig);

    }

    //获取用户信息
    public function getUser($openId)
    {
        return $this->officialAccount->user->get($openId);
    }
    //生成临时二维码
    public function tempQrCode($data)
    {
        return $this->officialAccount->qrcode->temporary($data, 6 * 24 * 3600);
    }
    //生成永久二维码
    public function foreverQrCode($data)
    {
        return $this->officialAccount->qrcode->forever($data); // 或者 $app->qrcode->forever("foo");
    }
    //获取上传图片
    public function uploadImage($filePath)
    {
        return $this->officialAccount->material->uploadImage($filePath);
    }

}
