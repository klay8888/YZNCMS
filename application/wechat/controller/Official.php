<?php
namespace app\wechat\controller;
use app\wechat\logic\OfficialLogic;
class Official extends Wechat
{
    protected function initialize()
    {
        parent::initialize();
        $this->officialLogic = new OfficialLogic();
    }

    public function index()
    {  
      if ($this->request->isAjax()){
          return $this->officialLogic->list();
       } 
       return $this->fetch();
    }

}
