<?php
namespace app\wechat\controller;

// 关注回复 逻辑
// 第一先从简单做起
// 假设模板固定这样他们操作起来也不乱，我的逻辑也省掉很多
// 不允许他们编辑类型   这样我只要固定判断就可以了

use app\wechat\logic\SubscribeLogic;

class Subscribe extends Wechat
{
    protected function initialize()
    {
        parent::initialize();
        $this->subscribeLogic = new SubscribeLogic();
    }
    public function index()
    {
        if ($this->request->isAjax()) {
            return $this->subscribeLogic->list();
        }
        return $this->fetch();
    }

    public function edit()
    {
        if ($this->request->isPost()) {
            $data   = $this->request->post('');
            $result = $this->validate($data, 'AdminUser.update');
            if (true !== $result) {
                return $this->error($result);
            }
            if ($this->Admin_User->editManager($data)) {
                $this->success("修改成功！");
            } else {
                $this->error($this->User->getError() ?: '修改失败！');
            }
        } else {
            $id   = $this->request->param('id/d');

            return $this->fetch();
        }
    }

}
