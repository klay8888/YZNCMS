<?php
namespace app\wechat\controller;

use app\common\controller\Base;
use think\facade\cache;
use app\wechat\service\wxHandler\EventHandler;
use app\wechat\service\wxHandler\TextHandler;
//自定义消息处理器必须是要满足php 版本大于7.1

class Platform extends Base
{
    public $wechatServer;
    public function __construct()
    {
        $this->openServer = app('openServer');
    }
    //公众号对第三方平台的事件
    public function initOauth()
    {
        $response = $this->openServer->openPlatform->server->serve();
        $response->send();
        exit; //直接响应不做处理
    }

    public function index($appid = '')
    {
        $officialAccount = $this->openServer->openPlatform->officialAccount($appid);//只要加入平台后全部都执行这个
        $this->handleMsg($officialAccount);
        $response = $officialAccount->server->serve();
        $response->send();exit;
    }
    //处理消息
    public function handleMsg($officialAccount)
    {

        $message = $officialAccount->server->getMessage();

        switch ($message['MsgType']) {
            case 'event':
                $officialAccount->server->push(EventHandler::class);
                break;
            case 'text':
                $officialAccount->server->push(TextHandler::class);
                break;
            // ... 其它消息
            default:
                return '其他消息';
                break;
        }
    }

    //代公众授权事件
    public function all($appid = '') //必须在当前函数响应 全网发布

    {
        $openPlatform = $this->openServer->openPlatform;
        $server       = $openPlatform->server; // 这里的 server 为授权方的 server，而不是开放平台的 server，请注意！！！
        $msg          = $server->getMessage();
        $gh_id        = $msg['ToUserName'];
        $openid       = $msg['FromUserName'];

        // 第三方测试用的
        var_dump($msg);exit();
        if ($msg['MsgType'] == 'text') {
            $word = $msg['Content'];

            if ($word == 'TESTCOMPONENT_MSG_TYPE_TEXT') {

                $officialAccount = $openPlatform->officialAccount($appid);

                $officialAccount->server->push(function ($message) {
                    return $message['Content'] . 'from_callback';
                });
                $response = $officialAccount->server->serve();
                $response->send();

            } elseif (strpos($word, 'QUERY_AUTH_CODE') == 0 && strpos($word, 'QUERY_AUTH_CODE') !== false) {
                echo '';
                $code           = substr($word, 16);
                $authorizerInfo = $openPlatform->handleAuthorize($code);
                // Log::error(strpos($word, 'QUERY_AUTH_CODE'));die;
                $authorizerInfo = $authorizerInfo['authorization_info'];
                // 设置信息缓存
                Cache::set($authorizerInfo['authorizer_appid'], $authorizerInfo['authorizer_refresh_token'], 300);
                $cuservice = $openPlatform->officialAccount($appid, $authorizerInfo['authorizer_refresh_token']);
                $cuservice->customer_service->message($code . "_from_api")
                    ->from($gh_id)
                    ->to($openid)
                    ->send();
            }

        }
        exit;
    }

}
