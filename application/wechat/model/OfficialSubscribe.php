<?php
namespace app\wechat\model;

use think\Model;

/**
 * Lhq 2020-07-29
 */
class OfficialSubscribe extends Model
{
    public function getTypeAttr($value)
    {
        $status = [ 0 => '默认关注', 1 => '下单后关注', 2 => '老用户关注'];
        return $status[$value];
    }
}
