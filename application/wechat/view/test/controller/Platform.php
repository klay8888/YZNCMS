<?php
namespace app\wechat\controller;

use app\common\controller\Base;
use EasyWeChat\Kernel\Messages\Text;

//自定义消息处理器必须是要满足php 版本大于7.1

class Platform extends Base
{
    public $wechatServer;
    public function __construct()
    {
        $this->openServer = app('openServer');
    }
    //公众号对第三方平台的事件
    public function init()
    {
        $response = $this->openServer->openPlatform->server->serve();
        $response->send();exit; //直接响应不做处理
    }
    //代公众授权事件
    public function index($appid = '')
    {
        $this->handleMsg($appid);
        // $response = $this->openServer->openPlatform->server->serve();
        // $response->send();exit; //直接响应不做处理
    }

    //处理消息
    public function handleMsg($appid)
    {
        $message = $this->openServer->openPlatform->server->getMessage();
        //$name    = $this->openServer->getOfficialName($appid);

        //全网发布
        if ($name == 'wxtest') {

            return $this->releaseToNetWork($this->openServer->openPlatform, $authorizer_appid, $message);
        }

        // switch ($message['MsgType']) {
        //     case 'event':
        //         $this->openServer->official[$name]->server->push('app\wechat\service\wxHandler\EventHandler' . $name); //不同的处理类
        //         break;
        //     case 'text':

        //         $s=$this->openServer->official[$name]->server->push('app\wechat\service\wxHandler\TextHandler' . $name);

        //         break;
        //     // ... 其它消息
        //     default:
        //         return '其他消息';
        //         break;
        // }
    }

    // 处理全网发布相关逻辑
    private function releaseToNetWork($open_platform, $authorizer_appid, $message)
    {

        //测试公众号使用客服消息接口处理用户消息 对应 返回Api文本消息检测
        if ($message['MsgType'] == 'text' && strpos($message['Content'], "QUERY_AUTH_CODE:") !== false) {
            $auth_code     = str_replace("QUERY_AUTH_CODE:", "", $message['Content']);
            $authorization = $open_platform->handleAuthorize($auth_code);
            ob_start();
            echo '==============1111=================';
            var_dump($authorization);
            $temp = ob_get_contents();
            ob_clean();
            file_put_contents('lbt.html', $temp, FILE_APPEND);
            $official_account_client = $open_platform->officialAccount($authorizer_appid, $authorization['authorization_info']['authorizer_refresh_token']);
            $content                 = $auth_code . '_from_api';
            $content                 = new Text($content);
            $official_account_client->customer_service->message($content)->to($message['FromUserName'])->send();

            //测试公众号处理用户消息
        } elseif ($message['MsgType'] == 'text' && $message['Content'] == 'TESTCOMPONENT_MSG_TYPE_TEXT') {
            $official_account_client = $open_platform->officialAccount($authorizer_appid);
            $official_account_client->server->push(function ($message) {
                return $message['Content'] . "_callback";
            });
            //发送事件消息
        } elseif ($message['MsgType'] == 'event') {
            $official_account_client = $open_platform->officialAccount($authorizer_appid);
            $official_account_client->server->push(function ($message) {
                return $message['Event'] . 'from_callback';
            });
        }
        $response = $official_account_client->server->serve();
        return $response;
    }

}
