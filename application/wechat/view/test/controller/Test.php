<?php
namespace app\wechat\controller;

use think\Db;

class Test
{

    public $workServer;
    public function __construct()
    {
        $this->workServer = app('workServer');
        $this->openServer = app('openServer');
    }
    //XingJiaHao 离职人员测试账户  7ede726a53be1606690aac6732612ab2 联系方式configid
    public function wx1()
    {

    }
    public function wx()
    {

         $url = $this->openServer->openPlatform->getPreAuthorizationUrl('http://g.huapus.com/wechat/Platform/init');
        $this->assign([
            'url' => $url,
        ]);
        // 模板输出
        return $this->fetch();

    }

    public function oauth()
    {
        $authCode = empty($_GET['auth_code']) ? "" : trim($_GET['auth_code']); //获取授权码
        $this->openServer->openPlatform->handleAuthorize($authCode);
        var_dump($this->openServer->openPlatform->getAuthorizers(0, 500));

    }

    /**
     * 更新企业微信通信录
     */
    public function updateWorkUserList()
    {
        $tmpList     = [];
        $workUserArr = Db::table('yzn_work_user')->column('userid');

        $departmentArr = $this->workServer->getDepartment();
        $followUsers = $this->workServer->getFollowUsers();
        foreach ($departmentArr['department'] as $key => $value) {
            $departmentUserArr = $this->workServer->getDepartmentUsers($value['id']);
            foreach ($departmentUserArr['userlist'] as $k => $v) {
                if (in_array($v['userid'], $workUserArr)) {
                    continue; //如果存在那么跳出此次循环
                }
                $tmpList[] = ['userid' => $v['userid'], 'name' => $v['name'], 'departmentid' => json_encode($v['department'])];

            }

        }

        return Db::table('yzn_work_user')->data($tmpList)->insertAll();
    }

    /**
     * @param  $userid 成员UserID。企业内必须唯一。不区分大小写，只能由数字、字母和“_-@.”四种字符组成，且第一个字符必须是数字或字母。
     * @param  $mobile 手机号
     */
    public function createWorkUser($userid, $mobile)
    {
        $data = [
            "userid"       => $userid,
            "name"         => "超哥",
            "english_name" => "overtrue",
            "mobile"       => $mobile,
            "department"   => "9",
        ];

        return $this->workServer->createUser($data);
    }
}
