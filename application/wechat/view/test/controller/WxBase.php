<?php
namespace app\wechat\controller;

use app\common\controller\Base;
use think\facade\Request;

class WxBase extends Base
{
    public $wechatServer;
    public $wxUser;

    public function __construct()
    {
        $this->wechatServer = app('wxServer');
        $this->initUser();
    }

    //初始话用户
    public function initUser()
    {

        if (empty($_SESSION['wxUser'])) {
            $oauth                  = $this->wechatServer->officialAccount->oauth;
            $_SESSION['target_url'] = Request::url(true); //回传地址
            $oauth->redirect()->send(); //调用授权回调页面
        }
        $this->wxUser = $_SESSION['wxUser']; //全局方便调用
    }

    //使用方式请 用php echo 输出 <?php echo
    //参数类型 array $APIs, $debug = false, $beta = false, $json = true
    public function initJssdk()
    {
        $APIs = ['chooseImage', 'uploadImage', 'downloadImage']; //js接口列表
        return $this->wechatServer->officialAccount->jssdk->buildConfig($APIs, false, false, true);
    }
}
