<?php
/*
 * author Lhq
 * 使用方法 在provider.php 里面绑定这个类 依赖注入后即可app()全局调用
 * composer 引入 composer require overtrue/wechat:~4.0 -vvv
 */
namespace app\wechat\service;

use EasyWeChat\Factory;

class workServer
{

    public $enterprise; //企业微信

    public function __construct()
    {
        //企业微信通用应用配置
        $enterpriseConfig = [
            'corp_id'       => 'wwc212091ff4fd4449',
            //'agent_id'      => ' ', // 如果有 agend_id 则填写
            'secret'        => 'PLhCEp7H72D0XHdC2ustaCFY-yY-nZzNm2XObEpN8sU',
            'token'         => 'n6AoBXfDV913UnxRWohh8HWpg7qpgNOs',
            'aes_key'       => 'bWycBFdRkVchiF3xzddnTBxsBUUDLCAweEBIGvSMG8E',
            'response_type' => 'array',
        ];
        $this->enterprise = Factory::work($enterpriseConfig);
        //企业微信通讯录应用配置
        $maillistConfig = [
            'corp_id'       => 'wwc212091ff4fd4449',
            //'agent_id'      => ' ', // 如果有 agend_id 则填写
            'secret'        => 'xZPpZqawYflIw5Vhm6YflKj5HGa6SdE3ft9SVsBzHy0',
            'token'         => 'n6AoBXfDV913UnxRWohh8HWpg7qpgNOs',
            'aes_key'       => 'bWycBFdRkVchiF3xzddnTBxsBUUDLCAweEBIGvSMG8E',
            'response_type' => 'array',
        ];
        $this->enterprise = Factory::work($enterpriseConfig);
        $this->maillist   = Factory::work($maillistConfig);

    }
    //创建企业用户
    public function createUser($data)
    {
        return $this->maillist->user->create($data);
    }
    //获取可联系外部用户的成员
    public function getFollowUsers()
    {
        return $this->enterprise->external_contact->getFollowUsers();
    }
    //添加联系我方式
    public function addContactWay($remark, $state, $userId)
    {
        //参数1是 联系方式 1 单人 2 多人 参数2是 联系方式 1小程序 2是二维码
        return $this->enterprise->contact_way->create(1, 2, ['remark' => $remark, 'state' => $state, 'user' => [$userId]]);
    }
    //更新联系我方式 之后要重新下载二维码图片
    public function updateContactWay($remark, $state, $configId)
    {
        return $this->enterprise->contact_way->update($configId, ['remark' => $remark, 'state' => $state]);
    }
    //获取联系我方式
    public function getContactWay($configId)
    {
        return $this->enterprise->contact_way->get($configId);
    }
    //获取客户列表
    public function getContactList($userId)
    {
        return $this->enterprise->external_contact->list($userId); //用户详情
    }
    //获取客户详情
    public function getExternalContact($externalUserId)
    {
        return $this->enterprise->external_contact->get($externalUserId); //用户详情
    }
    //获取所有企业微信用户
    public function getUser($userId)
    {
        return $this->enterprise->user->get($userId);
    }
    //获取部门列表
    public function getDepartment()
    {
        return $this->enterprise->department->list();
    }
    //递归获取部门下面成员
    public function getDepartmentUsers($departmentId)
    {
        return $this->enterprise->user->getDepartmentUsers($departmentId, true);
    }
    //获取标签列表
    public function getTagList($tag_id = null)
    {
        $token = $this->enterprise->access_token->getToken();
        $url   = 'https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_corp_tag_list?access_token=' . $token['access_token'];
        return $this->http_post($url, $tag_id);
    }
    //添加标签
    public function addTag($tagInfo)
    {
        $token = $this->enterprise->access_token->getToken();
        $url   = 'https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_corp_tag?access_token=' . $token['access_token'];
        return $this->http_post($url, $tag_id);
    }
    //删除标签
    public function delTag($tagInfo)
    {
        $token = $this->enterprise->access_token;
        $url   = 'https://qyapi.weixin.qq.com/cgi-bin/externalcontact/del_corp_tag?access_token=' . $token['access_token'];
        return $this->http_post($url, $value);
    }
    //给客户打标签
    public function markUserTag($tagInfo)
    {
        $token = $this->enterprise->access_token;
        $url   = 'https://qyapi.weixin.qq.com/cgi-bin/externalcontact/mark_tag?access_token=' . $token['access_token'];
        return $this->http_post($url, $value);
    }
    public function http_post($request_url, $parameters)
    {
        $parameters = json_encode($parameters);
        $curl       = curl_init();
        curl_setopt($curl, CURLOPT_URL, $request_url);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Content-Type:application/json",
        ));
        $response = curl_exec($curl);
        if (curl_error($curl)) {
            $error_msg = curl_error($curl);
            $error_no  = curl_errno($curl);
            curl_close($curl);
            throw new \Exception($error_msg, $error_no);
        }
        curl_close($curl);
        return json_decode($response, true);
    }
}
