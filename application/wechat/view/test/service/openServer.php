<?php
/*
 * author Lhq
 * 使用方法 在provider.php 里面绑定这个类 依赖注入后即可app()全局调用
 * composer 引入 composer require overtrue/wechat:~4.0 -vvv
 */
namespace app\wechat\service;

use \EasyWeChat\Factory;

class openServer
{
    public $officialArr; //公众号名字
    public $openPlatform; //开放平台
    public static $HUPU = 'wx401553255b893aed';

    public function __construct()
    {

        $openPlatformConfig = [
            'app_id'        => 'wx3f8db8c6dd056596',
            'secret'        => '57be5e7ddf178e30b42cf29cd195b8e3',
            'token'         => 'huapuscom',
            'response_type' => 'array',
            'aes_key'       => 'xdhpbhtjcs11lfjyswggp8vmxfzhxq5l6amt2hwbzn8',
            'oauth'         => [
                'scopes'   => ['snsapi_userinfo'], //回调授权级别
                'callback' => '/index/wechat/oauth', //回调地址
            ],
        ];

        // $this->officialAccount = Factory::officialAccount($hupuAccountConfig);
        // 代公众号实现业务
        $this->openPlatform     = Factory::openPlatform($openPlatformConfig);
        $this->officialArr      = ['wx401553255b893aed' => 'hupu', 'wx570bc396a51b8ff8' => 'wxtest'];
        $this->official['hupu'] = $this->openPlatform->officialAccount(self::$HUPU);

    }

    //获取用户信息
    public function getUser($openId, $name)
    {
        return $this->openPlatform[$name]->user->get($openId);
    }
    //生成临时二维码
    public function tempQrCode($data, $name)
    {
        return $this->openPlatform[$name]->qrcode->temporary($data, 6 * 24 * 3600);
    }
    //生成永久二维码
    public function foreverQrCode($data, $name)
    {
        return $this->openPlatform[$name]->qrcode->forever($data); // 或者 $app->qrcode->forever("foo");
    }
    //获取上传图片
    public function uploadImage($filePath, $name)
    {
        return $this->openPlatform[$name]->material->uploadImage($filePath);
    }
    //获取id 对应的名称
    public function getOfficialName($appid)
    {
        return $this->officialArr[$appid];
    }
}
