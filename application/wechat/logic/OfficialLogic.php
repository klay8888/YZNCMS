<?php
namespace app\wechat\logic;

use app\wechat\model\OfficialAccount;

/**
 * lhq 2020-07-28
 *
 */
class OfficialLogic
{

   public  function list() {

        $_list  = OfficialAccount::select();
        $total  = count($_list);
        $result = array("code" => 0, "count" => $total, "data" => $_list);
        return json($result);

    }
}
