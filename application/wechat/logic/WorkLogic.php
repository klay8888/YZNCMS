<?php
namespace app\wechat\logic;

use app\wechat\model\WorkCustomer;
use app\wechat\model\WorkCustomerTag;
use app\wechat\model\WorkUser;
use think\Db;

/**
 * lhq 2020-07-28
 * $reqUserData['userid']          = 'LiHuanQuan'; 我的测试信息
 *$reqUserData['external_userid'] = 'wmeFBtCQAAhDG26UDBmseIsfLdwGarzg';
 *$reqUserData['description']         = 'description';
 * $reqUserData['remark']         = 'remark';
 * $reqUserData['remark_mobiles']         = [['18899795895']];
 */
class WorkLogic
{

    public $workServer;
    public $token;
    public function __construct()
    {
        $this->workServer = app('workServer');
        $accessToken      = $this->workServer->enterprise->access_token;
        $this->token      = $accessToken->getToken()['access_token'];
    }

    /**
     * 更新企业微信通信录
     */
    public function updateWorkUserList()
    {
        $tmpList       = [];
        $workUserArr   = WorkUser::column('userid');
        $departmentArr = $this->workServer->getDepartment(); //部门列表
        $userInfo      = $this->workServer->getFollowUsers(); //具有外部联系人功能的成员
        foreach ($departmentArr['department'] as $key => $value) {
            $departmentUserArr = $this->workServer->getDepartmentUsers($value['id']); //递归获取部门下面所有人
            foreach ($departmentUserArr['userlist'] as $k => $v) {
                if (in_array($v['userid'], $workUserArr)) { //只获取新增的
                    continue; //如果存在那么跳出此次循环
                }
                $tmpList[] = ['userid' => $v['userid'], 'name' => $v['name'], 'departmentid' => json_encode($v['department'])];
            }
        }
        $tmpList = array_unique($tmpList, SORT_REGULAR); //获取所有部门详情的情况下可能会存在这样的情况 同一个用户在多个部门
        try {
            $WorkUser = new WorkUser();
            $WorkUser->saveAll($tmpList);
            WorkUser::where(['userid' => $userInfo['follow_user']])->update(['is_follow_user'=>'1']); //先生成后修改权限
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    /**
     * @param  $userid 成员UserID。企业内必须唯一。不区分大小写，只能由数字、字母和“_-@.”四种字符组成，且第一个字符必须是数字或字母。
     * @param  $mobile 手机号
     */
    public function createWorkUser($userName, $mobile)
    {
        $userid = uniqid();
        $data   = [
            "userid"     => $userid,
            "name"       => $userName,
            "mobile"     => $mobile,
            "department" => "16",
        ];

        $this->workServer->createUser($data);
        return true;
    }

    /**
     * @param  [userId] 用户uid
     * @param  [state] 自定义参数
     * 创建新的活码
     */
    public function createUserQrCode($userId, $state)
    {
        $this->workServer->addContactWay($userId, $state);
        $this->workServer->getContactWay($configId);
    }

    /**
     * @param  [userId] 用户uid
     * @param  [state] 自定义参数
     * 修改活码数据
     */
    public function editUserQrCode($configId, $state)
    {
        $this->workServer->updateContactWay($configId, $state);
        $this->workServer->getContactWay($configId);
    }

    /**
     * [editUserInfo description] 修改用户信息 同步标签的话还是考虑后面批量重新获取
     * @param  [type] $type [description]
     * @return [type]       [description]
     */
    public function editUserInfo($type)
    {
        var_dump("expression");
        if ($type == 1) {
            //在小鹅通上面有消费的记录
            $sql = "  SELECT customer.uid, customer.belongid ,customer.unionid,h.amount ,customer.tagnames FROM yzn_work_customer  as customer LEFT JOIN yzn_hua  as h ON  customer.unionid =h.unionid WHERE customer.unionid =h.unionid ";
        }
        if ($type == 2) {
            //在小鹅通上面没有消费的记录
            $sql = "  SELECT customer.uid, customer.belongid ,customer.unionid ,customer.tagnames,0 as amount FROM yzn_work_customer  as customer LEFT JOIN yzn_hua  as h ON  customer.unionid =h.unionid WHERE h.uid  is NULL and ";
        }
        $tagList = [
            'eteFBtCQAAnXztCBahD_WDiVKFUBu6Tw', //0
            'eteFBtCQAAihiEbqfcyzPl7n-NA2sN-Q', //0-150
            'eteFBtCQAAmk-zzucrphDQ2yP3mE0FIg', //150-400
            'eteFBtCQAA28_zXp8hVP6Q7O-M3lhMlA', //400-800
            'eteFBtCQAAovC3Jm17TDrddKgPB_9qdA', //800-2500
            'eteFBtCQAAsmtQ3RXjQiXRfMq08rppeg', //2500

        ];
        $follow_user = Db::query($sql);
        foreach ($follow_user as $key => $value) {
            $reqData['userid']          = $value['belongid'];
            $reqData['external_userid'] = $value['uid'];
            $reqTagData                 = $reqData;
            $reqUserData                = $reqData;
            $amount                     = (int) $value['amount'];

            if (!empty($value['tagnames'])) {
                $tagArr = explode(',', $value['tagnames']);
                if (in_array('曾乔正价课', $tagArr)) {
                    $amount = $amount - 1980; //在里面
                }
            }
            switch ((int) $amount) {
                case $amount > 2500:
                    $tag                   = 5;
                    $reqTagData['add_tag'] = ['eteFBtCQAAsmtQ3RXjQiXRfMq08rppeg'];
                    $tagList               = array_splice($tagList, 5, 1); //移除其他四个
                    break;
                case $amount >= 800 && $amount < 2500:
                    $tag                   = 4;
                    $reqTagData['add_tag'] = ['eteFBtCQAAovC3Jm17TDrddKgPB_9qdA'];
                    $tagList               = array_splice($tagList, 4, 1); //移除其他四个

                    break;
                case $amount >= 400 && $amount < 800:
                    $tag                   = 3;
                    $reqTagData['add_tag'] = ['eteFBtCQAA28_zXp8hVP6Q7O-M3lhMlA'];
                    $tagList               = array_splice($tagList, 3, 1); //移除其他四个
                    break;
                case $amount >= 150 && $amount < 400:
                    $tag                   = 2;
                    $reqTagData['add_tag'] = ['eteFBtCQAAmk-zzucrphDQ2yP3mE0FIg'];
                    $tagList               = array_splice($tagList, 2, 1); //移除其他四个
                    break;
                case $amount > 0 && $amount < 150:
                    $tag                   = 1;
                    $reqTagData['add_tag'] = ['eteFBtCQAAihiEbqfcyzPl7n-NA2sN-Q'];
                    $tagList               = array_splice($tagList, 1, 1); //移除其他四个
                    break;
                default:
                    $tag                   = 0;
                    $reqTagData['add_tag'] = ['eteFBtCQAAnXztCBahD_WDiVKFUBu6Tw']; //默认消费未空的
                    $tagList               = array_splice($tagList, 0, 1); //移除其他四个
            }
            $reqTagData['remove_tag'] = $tagList;
            $reqUserData['remark']    = '已消费' . $value['amount'];
            var_dump(https_request('https://qyapi.weixin.qq.com/cgi-bin/externalcontact/mark_tag?access_token=' . $this->token, $reqTagData, 'json')); //打标签
            var_dump(https_request('https://qyapi.weixin.qq.com/cgi-bin/externalcontact/remark?access_token=' . $this->token, $reqUserData, 'json')); //修改备注
            exit();
        }
    }

    //同步标签
    public function synchroTags()
    {
        $workCustomerTag = new WorkCustomerTag();
        $customerTagArr  = WorkCustomerTag::column('tagid');

        $tags    = json_decode(https_request('https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_corp_tag_list?access_token=' . $this->token, 's'), true);
        $tmpList = [];
        foreach ($tags['tag_group'] as $key => $value) {
            foreach ($value['tag'] as $k => $v) {
                if (in_array($v['id'], $customerTagArr)) { //只获取新增的
                    continue; //如果存在那么跳出此次循环
                }
                $tmpList[] = ['groupid' => $value['group_id'], 'name' => $v['name'], 'tagid' => $v['id']];
            }
        }
        $workCustomerTag->saveAll($tmpList);
    }

/**
 * 获取企业微信客户基本信息 插入到表
 */
    public function getWorkCustomer()
    {

        $date        = date('Y-m-d h:i:s', time());
        $workUserArr = WorkUser::column('userid');

        foreach ($workUserArr as $key => $value) {
            $contactList = $this->workServer->getContactList($value); //获取企业微信用户下面所有人

            $workCustomerArr = WorkCustomer::column('uid'); //每次进来都获取新值

            if ($contactList['errcode'] == 0 && !empty($contactList)) {
                //有外部联系人才进来
                //该账户下面有客户
                $tmpList = []; //每次重置
                foreach ($contactList['external_userid'] as $k => $v) {

                    if (in_array($v, $workCustomerArr)) {
                        //只获取新增的
                        var_dump($v);
                        echo "string";
                        continue; //如果存在那么跳出此次循环
                    }

                    $tmpList[] = ['uid' => $v, 'belongid' => $value, 'is_add' => 1, 'create_time' => $date, 'update_time' => $date];
                }
                $workCustomer = new WorkCustomer(); //每个老师都插入一次更新
                $workCustomer->saveAll($tmpList);
                sleep(5); //插入需要时间延迟五秒
            }

        }

    }

/**
 * 获取企业微信客户基本信息 更新
 */
    public function editCustomer()
    {

        $workUserArr = WorkCustomer::column('uid');

        foreach ($workUserArr as $key => $value) {
            $userInfo = $this->workServer->getExternalContact($value);
            $tagsname = '';

            if (!empty($userInfo['follow_user'][0]['tags'])) {
                foreach ($userInfo['follow_user'][0]['tags'] as $k => $v) {
                    if ($v['type'] == 1) {
                        $tagsname .= $v['tag_name'] . ',';
                    }
                    var_dump(WorkCustomer::where('uid', $value)->update(['tagnames' => $tagsname, 'unionid' => $userInfo['external_contact']['unionid']]));
                }
            }
            var_dump(WorkCustomer::where('uid', $value)->update(['unionid' => $userInfo['external_contact']['unionid']]));
        }

    }

    /**
     * 更新是否添加了企业微信
     * 5kcrm_crm_customer openid = hema_store_user  yzn_work_customer
     *
     * SELECT crm_c.openid , crm_c.customer_id FROM 5kcrm_crm_customer AS crm_c LEFT JOIN hema_store_user AS store_c ON crm_c.openid =store_c.wechat_open_id LEFT JOIN  yzn_work_customer as work_c ON  store_c.union_id = work_c.unionid WHERE
    crm_c.openid   <> '' ;
     */
    public function updateCustomerIsFriend()
    {
        $sql         = "SELECT crm_c.openid , crm_c.customer_id FROM 5kcrm_crm_customer AS crm_c LEFT JOIN hema_store_user AS store_c ON crm_c.openid =store_c.wechat_open_id LEFT JOIN  yzn_work_customer as work_c ON  store_c.union_id = work_c.unionid WHERE crm_c.openid   <> '' ";
        $follow_user = Db::query($sql);
        foreach ($follow_user as $key => $value) {
            var_dump(Db::table('5kcrm_crm_customer')->update(['is_wrok' => '1', 'customer_id' => $value['customer_id']]));

        }

    }

}

//根据训练营课程 减去价格再打标签
