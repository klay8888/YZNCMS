<?php 
/**
 * lhq 2020-07-28
 * 
 */
class wxWork 
{
	
	function __construct(argument)
	{
		# code...
	}

	    /**
     * 更新企业微信通信录
     */
    public function updateWorkUserList()
    {
        $tmpList       = [];
        $workUserArr   = Db::table('yzn_work_user')->column('userid');

        $departmentArr = $this->workServer->getDepartment();
        foreach ($departmentArr['department'] as $key => $value) {
            $departmentUserArr = $this->workServer->getDepartmentUsers($value['id']);
            foreach ($departmentUserArr['userlist'] as $k => $v) {
                if (in_array($v['userid'], $workUserArr)) {
                    continue; //如果存在那么跳出此次循环
                }
                $tmpList[] = ['userid' => $v['userid'], 'name' => $v['name'], 'departmentid' => json_encode($v['department'])];

            }

        }

      return  Db::table('yzn_work_user')->data($tmpList)->insertAll();
    }

    /**
     * @param  $userid 成员UserID。企业内必须唯一。不区分大小写，只能由数字、字母和“_-@.”四种字符组成，且第一个字符必须是数字或字母。
     * @param  $mobile 手机号
     */
    public function createWorkUser($userid,$mobile)
    {
        $data = [
            "userid"       => $userid,
            "name"         => "超哥",
            "english_name" => "overtrue",
            "mobile"       => $mobile,
            "department"   => "9",
        ];

       return  $this->workServer->createUser($data);
    }
}