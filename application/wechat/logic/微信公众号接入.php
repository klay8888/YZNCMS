<?php
namespace app\wechat\controller;

use app\common\controller\Base;
//自定义消息处理器必须是要满足php 版本大于7.1

use app\wechat\service\wxHandler\EventHandler;
use app\wechat\service\wxHandler\TextHandler;

class Official extends Base
{
    public $wechatServer;
    public function __construct()
    {
        $this->wechatServer = app('wxServer');
    }

    public function init()
    {
        // $this->handleMsg();
        // $response = $this->wechatServer->officialAccount->server->serve();
        // $response->send();exit;
    }
    public function index()
    {
        $this->handleMsg();
        $response = $this->wechatServer->officialAccount->server->serve();
        $response->send();exit;
    }

// 获取 OAuth 授权结果用户信息 同时插入数据库
    public function oauth()
    {
        $oauth                   = $this->wechatServer->officialAccount->oauth;
        $user                    = $oauth->user();
        $_SESSION['wechat_user'] = $user->toArray();
        $targetUrl               = empty($_SESSION['target_url']) ? '/' : $_SESSION['target_url'];
        header('location:' . $targetUrl);
    }

 

}
