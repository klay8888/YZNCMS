<?php

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use app\wechat\logic\WorkLogic;

class Editwork extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('editwork');
        // 设置参数
        
    }

    protected function execute(Input $input, Output $output)
    {
    	// 指令输出
    	$output->writeln('editwork');
        $workLogic = new WorkLogic();
        $workLogic->updateWorkUserList();
    }
}
