<?php
// +----------------------------------------------------------------------
// | Yzncms [ 御宅男工作室 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2007 http://yzncms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 御宅男 <530765310@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\AdminUser as Admin_User;
use app\admin\model\AuthGroup as AuthGroup_Model;
use app\common\controller\Adminbase;
use think\Db;

/**
 * 管理员管理
 */
class Manager extends Adminbase
{
    protected function initialize()
    {
        parent::initialize();
        $this->Admin_User = new Admin_User;
        $this->AuthGroup_Model = new AuthGroup_Model();
    }

    /**
     * 管理员管理列表
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $_list = $this->Admin_User
                ->order(array('id' => 'ASC'))
                ->withAttr('roleid', function ($value, $data) {
                    return $this->AuthGroup_Model->getRoleIdName($value);
                })
                ->select();
            $total = count($_list);
            $result = array("code" => 0, "count" => $total, "data" => $_list);
            return json($result);
        }
        return $this->fetch();
    }

    /**
     * 添加管理员
     */
    public function add()
    {
        
        if ($this->request->isPost()) {
            $data = $this->request->post('');
            $result = $this->validate($data, 'AdminUser.insert');
            if (true !== $result) {
                return $this->error($result);
            }
            if ($this->Admin_User->createManager($data)) {
                $this->success("添加管理员成功！", url('admin/manager/index'));
            } else {
                $error = $this->Admin_User->getError();
                $this->error($error ? $error : '添加失败！');
            }
        } else {
            $data_follow_man_id     =$this->Admin_User->where(['is_follow_man' => 1])->field('id,nickname')->select()->toArray();
            $data_promotion_man_id  =$this->Admin_User->where(['is_promotion_man' => 1])->field('id,nickname')->select()->toArray();
            $data_work_user         =Db::query("select * from yzn_work_user");

            $this->assign("follow_man_id", $data_follow_man_id);
            $this->assign("promotion_man_id", $data_promotion_man_id);
            $this->assign("data_work_user", $data_work_user);
            $this->assign("roles", model('admin/AuthGroup')->getGroups());
            return $this->fetch();
        }
    }

    /**
     * 管理员编辑
     */
    public function edit()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post('');
            $result = $this->validate($data, 'AdminUser.update');
            if (true !== $result) {
                return $this->error($result);
            }
            if ($this->Admin_User->editManager($data)) {
                $this->success("修改成功！");
            } else {
                $this->error($this->User->getError() ?: '修改失败！');
            }
        } else {
            $id = $this->request->param('id/d');
            $data = $this->Admin_User->where(array("id" => $id))->find();
            if (empty($data)) {
                $this->error('该信息不存在！');
            }

            $data_follow_man_id     =$this->Admin_User->where(['is_follow_man' => 1])->field('id,nickname')->select()->toArray();
            $data_promotion_man_id  =$this->Admin_User->where(['is_promotion_man' => 1])->field('id,nickname')->select()->toArray();
            $data_work_user         =Db::query("select * from yzn_work_user");
//            echo "<pre>";
//            print_r($data_work_user);exit;
            $this->assign("follow_man_id", $data_follow_man_id);
            $this->assign("promotion_man_id", $data_promotion_man_id);
            $this->assign("data_work_user", $data_work_user);

            $this->assign("data", $data);
            $this->assign("roles", model('admin/AuthGroup')->getGroups());
            return $this->fetch();
        }
    }

    /**
     * 管理员删除
     */
    public function del()
    {
        $id = $this->request->param('id/d');
        if ($this->Admin_User->deleteManager($id)) {
            $this->success("删除成功！");
        } else {
            $this->error($this->Admin_User->getError() ?: '删除失败！');
        }
    }

    //批量更新.
//    public function multi()
//    {
//        // 管理员禁止批量操作
//        $this->error();
//    }

//    public function multi()
//    {
//        cache("Model", null);
//        return parent::multi2();
//    }

    public function multi()
    {

         $arr = $this->request->param();
         $field = $arr['param'];
         $bool = $this->Admin_User->where(['id' => $arr['id']])->update([$field=>$arr['value']]);
         if($bool){
            $this->success("操作成功");
         }

         $this->success("操作失败");
    }





}
