<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: yunwuxin <448901948@qq.com>
// +----------------------------------------------------------------------
//数组形式--
//         ob_start();
// echo '==============1111=================';
// var_dump($message);
// $temp=ob_get_contents();
// ob_clean();
// file_put_contents('lbt.html',$temp,FILE_APPEND);
return [
    'wxServer'=>app\wechat\service\wxServer::class,
    'workServer'=>app\wechat\service\workServer::class,
    'openServer'=>app\wechat\service\openServer::class,
];
